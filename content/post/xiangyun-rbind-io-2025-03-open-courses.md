---
title: B 站公开课
date: '2025-03-01'
linkTitle: https://xiangyun.rbind.io/2025/03/open-courses/
source: 黄湘云 on Xiangyun Huang | 黄湘云
description: 自研究生毕业以后，很少从头到尾地去刷专业课程、讲座访谈，一直在吃老本，也很少在本专业以外拓展。下面主要从 B ...
disable_comments: true
---
自研究生毕业以后，很少从头到尾地去刷专业课程、讲座访谈，一直在吃老本，也很少在本专业以外拓展。下面主要从 B ...