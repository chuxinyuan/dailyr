---
title: blocklength 0.2.0
date: '2025-02-17'
linkTitle: https://www.r-bloggers.com/2025/02/blocklength-0-2-0/
source: R-bloggers
description: |-
  <div style = "width:60%; display: inline-block; float:left; ">
  I’m excited to announce that blocklength 0.2.0 is now available on CRAN! blocklength is designed to be used with block-bootstrap procedures and makes it quick and easy to select a block-length quantitatively. This significant update includes a new blo...</div>
  <div style = "width: 40%; display: inline-block; float:right;"></div>
  <div style="clear: both;"></div>
  <strong>Continue reading</strong>: <a href="https://www.r-bloggers.com/2025/02/blocklength-0-2-0/">blocklength ...
disable_comments: true
---
<div style = "width:60%; display: inline-block; float:left; ">
I’m excited to announce that blocklength 0.2.0 is now available on CRAN! blocklength is designed to be used with block-bootstrap procedures and makes it quick and easy to select a block-length quantitatively. This significant update includes a new blo...</div>
<div style = "width: 40%; display: inline-block; float:right;"></div>
<div style="clear: both;"></div>
<strong>Continue reading</strong>: <a href="https://www.r-bloggers.com/2025/02/blocklength-0-2-0/">blocklength ...