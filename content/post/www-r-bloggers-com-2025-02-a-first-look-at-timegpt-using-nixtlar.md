---
title: A First Look at TimeGPT using nixtlar
date: '2025-02-12'
linkTitle: https://www.r-bloggers.com/2025/02/a-first-look-at-timegpt-using-nixtlar/
source: R-bloggers
description: |-
  <div style = "width:60%; display: inline-block; float:left; ">
  <p>This post is a first look at Nixtla’s TimeGPT generative, pre-trained transformer for time series forecasting using the nixtlar R package.<br />
  As described in Garza et al. (2021), TimeGPT is a Transformer-based time series model with self-atten...</p></div>
  <div style = "width: 40%; display: inline-block; float:right;"></div>
  <div style="clear: both;"></div>
  <strong>Continue reading</strong>: <a href="https://www.r-bloggers.com/2025/02/a-first-look-at-timegpt-using-nixtlar/">A First Look at TimeGPT using ...
disable_comments: true
---
<div style = "width:60%; display: inline-block; float:left; ">
<p>This post is a first look at Nixtla’s TimeGPT generative, pre-trained transformer for time series forecasting using the nixtlar R package.<br />
As described in Garza et al. (2021), TimeGPT is a Transformer-based time series model with self-atten...</p></div>
<div style = "width: 40%; display: inline-block; float:right;"></div>
<div style="clear: both;"></div>
<strong>Continue reading</strong>: <a href="https://www.r-bloggers.com/2025/02/a-first-look-at-timegpt-using-nixtlar/">A First Look at TimeGPT using ...