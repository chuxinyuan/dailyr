---
title: How well prepared are we to rapidly analyse a new influenza pandemic? A brief
  perspective on analysis conducted for UK government advisory groups during COVID-19
date: '2024-12-23'
linkTitle: https://www.r-bloggers.com/2024/12/how-well-prepared-are-we-to-rapidly-analyse-a-new-influenza-pandemic-a-brief-perspective-on-analysis-conducted-for-uk-government-advisory-groups-during-covid-19/
source: R-bloggers
description: |-
  <div style = "width:60%; display: inline-block; float:left; ">
  <p>With multiple reports of influenza H5N1 cases that have no clear animal exposure, it is useful to consider what kinds of analysis would be required, and how easily this could be performed. As a starting point, this post reflects on some of the r...</p></div>
  <div style = "width: 40%; display: inline-block; float:right;"></div>
  <div style="clear: both;"></div>
  <strong>Continue reading</strong>: <a href="https://www.r-bloggers.com/2024/12/how-well-prepared-are-we-to-rapidly-analyse-a-new-influenza-pandemic-a-brief-perspective-on-ana ...
disable_comments: true
---
<div style = "width:60%; display: inline-block; float:left; ">
<p>With multiple reports of influenza H5N1 cases that have no clear animal exposure, it is useful to consider what kinds of analysis would be required, and how easily this could be performed. As a starting point, this post reflects on some of the r...</p></div>
<div style = "width: 40%; display: inline-block; float:right;"></div>
<div style="clear: both;"></div>
<strong>Continue reading</strong>: <a href="https://www.r-bloggers.com/2024/12/how-well-prepared-are-we-to-rapidly-analyse-a-new-influenza-pandemic-a-brief-perspective-on-ana ...