---
title: Monte Carlo [exam]
date: '2025-01-21'
linkTitle: https://www.r-bloggers.com/2025/01/monte-carlo-exam/
source: R-bloggers
description: |-
  <div style = "width:60%; display: inline-block; float:left; "> My final exam for the Monte Carlo course I taught last semester proved too much of a challenge for my fourth year students, despite being rather elementary and centred on accept-reject algorithms and importance/bridge sampling. One of the problems was a decomposition of the truncated Normal simulation method proposed ...</div>
  <div style = "width: 40%; display: inline-block; float:right;"></div>
  <div style="clear: both;"></div>
  <strong>Continue reading</strong>: <a href="https://www.r-bloggers.com/2025/01/monte-carlo-exam/">Monte ...
disable_comments: true
---
<div style = "width:60%; display: inline-block; float:left; "> My final exam for the Monte Carlo course I taught last semester proved too much of a challenge for my fourth year students, despite being rather elementary and centred on accept-reject algorithms and importance/bridge sampling. One of the problems was a decomposition of the truncated Normal simulation method proposed ...</div>
<div style = "width: 40%; display: inline-block; float:right;"></div>
<div style="clear: both;"></div>
<strong>Continue reading</strong>: <a href="https://www.r-bloggers.com/2025/01/monte-carlo-exam/">Monte ...