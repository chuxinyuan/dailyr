---
title: registration open for BayesComp 2025
date: '2025-01-28'
linkTitle: https://www.r-bloggers.com/2025/01/registration-open-for-bayescomp-2025/
source: R-bloggers
description: |-
  <div style = "width:60%; display: inline-block; float:left; ">
  The registration for the incoming, exciting, Bayes Comp 2025 conference (and its satellites) is now open, including information regarding accommodations for the conference. Early bird rates run till 15 March. Furthermore, the call for contributed talk...</div>
  <div style = "width: 40%; display: inline-block; float:right;"></div>
  <div style="clear: both;"></div>
  <strong>Continue reading</strong>: <a href="https://www.r-bloggers.com/2025/01/registration-open-for-bayescomp-2025/">registration open for BayesComp ...
disable_comments: true
---
<div style = "width:60%; display: inline-block; float:left; ">
The registration for the incoming, exciting, Bayes Comp 2025 conference (and its satellites) is now open, including information regarding accommodations for the conference. Early bird rates run till 15 March. Furthermore, the call for contributed talk...</div>
<div style = "width: 40%; display: inline-block; float:right;"></div>
<div style="clear: both;"></div>
<strong>Continue reading</strong>: <a href="https://www.r-bloggers.com/2025/01/registration-open-for-bayescomp-2025/">registration open for BayesComp ...