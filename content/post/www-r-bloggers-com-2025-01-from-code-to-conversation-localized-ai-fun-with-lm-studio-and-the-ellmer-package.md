---
title: 'From code to conversation: Localized AI fun with LM Studio and the ellmer
  package'
date: '2025-01-19'
linkTitle: https://www.r-bloggers.com/2025/01/from-code-to-conversation-localized-ai-fun-with-lm-studio-and-the-ellmer-package/
source: R-bloggers
description: |-
  <div style = "width:60%; display: inline-block; float:left; "> Summary: in this post I demonstrate how you can interact with locally hosted LLM models in R using the ellmer package and LM Studio.</div>
  <div style = "width: 40%; display: inline-block; float:right;"></div>
  <div style="clear: both;"></div>
  <strong>Continue reading</strong>: <a href="https://www.r-bloggers.com/2025/01/from-code-to-conversation-localized-ai-fun-with-lm-studio-and-the-ellmer-package/">From code to conversation: Localized AI fun with LM Studio and the ellmer ...
disable_comments: true
---
<div style = "width:60%; display: inline-block; float:left; "> Summary: in this post I demonstrate how you can interact with locally hosted LLM models in R using the ellmer package and LM Studio.</div>
<div style = "width: 40%; display: inline-block; float:right;"></div>
<div style="clear: both;"></div>
<strong>Continue reading</strong>: <a href="https://www.r-bloggers.com/2025/01/from-code-to-conversation-localized-ai-fun-with-lm-studio-and-the-ellmer-package/">From code to conversation: Localized AI fun with LM Studio and the ellmer ...